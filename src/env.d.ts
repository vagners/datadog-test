declare global {
  namespace NodeJS {
    interface ProcessEnv {
      REACT_APP_ENVIRONMENT: string
      REACT_APP_VERSION: string

      REACT_APP_DATADOG_APP_ID: string
      REACT_APP_DATADOG_CLIENT_TOKEN: string
      REACT_APP_DATADOG_SERVICE: string
    }
  }
}

export {}
