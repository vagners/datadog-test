import { Component, ReactElement } from 'react'
import { datadogRum } from '@datadog/browser-rum'

type Props = {
  children: ReactElement
}

type State = {
  hasError: boolean
}

export class ErrorBoundary extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = { hasError: false }
  }

  static getDerivedStateFromError () {
    return { hasError: true }
  }

  componentDidCatch(error: any) {
    datadogRum.addError(error)
  }

  render () {
    if (this.state.hasError) {
      return <h1>Something went wrong.</h1>
    }

    return this.props.children
  }
}
